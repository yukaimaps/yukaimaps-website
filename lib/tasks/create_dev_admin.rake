task :create_dev_admin => :environment do
  admin = User.create!(
    :email=>"admin@example.com",
    :email_confirmation=>"admin@example.com",
    :display_name=>"Admin",
    :pass_crypt=>"password",
    :pass_crypt_confirmation=>"password",
    :data_public=>true,
    :terms_seen=>true,
    :terms_agreed=>DateTime.now,
    :tou_agreed=>DateTime.now,
  )
  admin.activate!
  admin.roles.create(:role=>"administrator", :granter_id => admin.id)
  admin.roles.create(:role=>"moderator", :granter_id => admin.id)
  admin.save!

  Oauth2Application.create!(
    :name=>"iD",
    :confidential=>false,
    :uid=>"123456789",
    :scopes=>"read_prefs write_prefs write_api read_gpx write_notes",
    :secret=>Doorkeeper::SecretStoring::Sha256Hash.transform_secret("987654321"),
    :redirect_uri=>"http://127.0.0.1:8080/land.html",
    :owner=>admin,
  )
end

