class ApiController < ApplicationController
  skip_before_action :verify_authenticity_token

  private

  def oidc_config
    return {
      :algorithms => Rails.application.config_for(:oidc)[:algorithms],
      :jwks =>  JSON.parse(Rails.application.config_for(:oidc)[:certs])
    }
  end

  def decode_oidc_token(token)
    decoded_token = JWT.decode(token, nil, true, oidc_config)
    if decoded_token
      return decoded_token[0]
    end
    return nil
  end

  def get_oidc_user(user_data)
    user_auth_provider = user_data['iss']
    user_auth_uid = user_data['sub']
    # email is unique and not null in OSM users db,
    # so use the OIDC sub if no email
    user_email = user_data.fetch('email', "#{user_auth_uid}:#{user_auth_provider}")
    user_display_name = user_data.fetch('name', user_data.fetch('preferred_username', user_email))

    oidc_user = User.find_or_create_by('email': user_email) do |user|
      auth_provider = user_auth_provider
      auth_uid = auth_uid
      display_name = user_display_name
      user.pass_crypt = ''
      user.pass_crypt_confirmation = ''
      user.terms_seen = true
      user.terms_agreed = DateTime.now
      user.tou_agreed = DateTime.now
      user.data_public = true
    end
    if oidc_user.auth_provider != user_auth_provider
      oidc_user.auth_provider = user_auth_provider
    end
    if oidc_user.auth_uid != user_auth_uid
      oidc_user.auth_uid = user_auth_uid
    end
    if oidc_user.display_name != user_display_name
      oidc_user.display_name = user_display_name
    end
    if oidc_user.email != user_email
      oidc_user.email = user_email
    end
    begin
      oidc_user.save(validate: false)
    rescue => e
      logger.error "Error saving user #{e.message}"
    end
    if !oidc_user.new_record?
      if !oidc_user.active?
        oidc_user.activate!
      end
      return oidc_user
    end
    return nil
  end

  ##
  # Set allowed request formats if no explicit format has been
  # requested via a URL suffix. Allowed formats are taken from
  # any HTTP Accept header with XML as the default.
  def set_request_formats
    unless params[:format]
      accept_header = request.headers["HTTP_ACCEPT"]

      if accept_header
        # Some clients (such asJOSM) send Accept headers which cannot be
        # parse by Rails, for example:
        #
        #   Accept: text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2
        #
        # where both "*" and ".2" as a quality do not adhere to the syntax
        # described in RFC 7231, section 5.3.1, etc.
        #
        # As a workaround, and for back compatibility, default to XML format.
        mimetypes = begin
          Mime::Type.parse(accept_header)
        rescue Mime::Type::InvalidMimeType
          Array(Mime[:xml])
        end

        # Allow XML and JSON formats, and treat an all formats wildcard
        # as XML for backwards compatibility - all other formats are discarded
        # which will result in a 406 Not Acceptable response being sent
        formats = mimetypes.map do |mime|
          if mime.symbol == :xml || mime == "*/*" then :xml
          elsif mime.symbol == :json then :json
          end
        end
      else
        # Default to XML if no accept header was sent - this includes
        # the unit tests which don't set one by default
        formats = Array(:xml)
      end

      request.formats = formats.compact
    end
  end

  def authorize(realm = "Web Password", errormessage = "Couldn't authenticate you")
    # make the current_user object from any auth sources we have
    setup_user_auth

    # handle authenticate pass/fail
    unless current_user
      # no auth, the user does not exist or the password was wrong
      if Settings.basic_auth_support
        response.headers["WWW-Authenticate"] = "Basic realm=\"#{realm}\""
        render :plain => errormessage, :status => :unauthorized
      else
        render :plain => errormessage, :status => :forbidden
      end

      false
    end
  end

  def current_ability
    # Use capabilities from the oauth token if it exists and is a valid access token
    if doorkeeper_token&.accessible?
      ApiAbility.new(nil).merge(ApiCapability.new(doorkeeper_token))
    elsif Authenticator.new(self, [:token]).allow?
      ApiAbility.new(nil).merge(ApiCapability.new(current_token))
    else
      ApiAbility.new(current_user)
    end
  end

  def deny_access(_exception)
    if doorkeeper_token || current_token
      set_locale
      report_error t("oauth.permissions.missing"), :forbidden
    elsif current_user
      head :forbidden
    elsif Settings.basic_auth_support
      realm = "Web Password"
      errormessage = "Couldn't authenticate you"
      response.headers["WWW-Authenticate"] = "Basic realm=\"#{realm}\""
      render :plain => errormessage, :status => :unauthorized
    else
      render :plain => errormessage, :status => :forbidden
    end
  end

  def gpx_status
    status = database_status
    status = "offline" if status == "online" && Settings.status == "gpx_offline"
    status
  end

  def oidc_authenticate(oidc_token)
    user_data = decode_oidc_token(oidc_token)
    if user_data
      oidc_user = get_oidc_user(user_data)
      if oidc_user
        logger.info "User logged in with oidc token"
        return oidc_user
      end
    end
  end

  ##
  # sets up the current_user for use by other methods. this is mostly called
  # from the authorize method, but can be called elsewhere if authorisation
  # is optional.
  def setup_user_auth
    logger.info "setup_user_auth"
    # try and setup using OAuth
    if doorkeeper_token&.accessible?
      self.current_user = User.find(doorkeeper_token.resource_owner_id)
    elsif Authenticator.new(self, [:token]).allow?
      # self.current_user setup by OAuth
    elsif request.headers.fetch('Authorization', '').starts_with? "Bearer "
      oidc_token = request.headers['Authorization'].split('Bearer ')[1]
      self.current_user = oidc_authenticate(oidc_token)
    elsif Settings.basic_auth_support
      username, passwd = auth_data # parse from headers
      # authenticate per-scheme
      self.current_user = if username.nil?
                            nil # no authentication provided - perhaps first connect (client should retry after 401)
                          elsif username == "token"
                            User.authenticate(:token => passwd) # preferred - random token for user from db, passed in basic auth
                          elsif username == "access_token"
                            # Basic auth with 'access_token' as username, and a valid
                            # OIDC access token as password.
                            oidc_authenticate(passwd)
                          else
                            User.authenticate(:username => username, :password => passwd) # basic auth
                          end
      # log if we have authenticated using basic auth
      logger.info "Authenticated as user #{current_user.id} using basic authentication" if current_user
    end

    # have we identified the user?
    if current_user
      # check if the user has been banned
      user_block = current_user.blocks.active.take
      unless user_block.nil?
        set_locale
        if user_block.zero_hour?
          report_error t("application.setup_user_auth.blocked_zero_hour"), :forbidden
        else
          report_error t("application.setup_user_auth.blocked"), :forbidden
        end
      end

      # if the user hasn't seen the contributor terms then don't
      # allow editing - they have to go to the web site and see
      # (but can decline) the CTs to continue.
      if !current_user.terms_seen && flash[:skip_terms].nil?
        set_locale
        report_error t("application.setup_user_auth.need_to_see_terms"), :forbidden
      end
    end
  end
end
